<?php
/**
 * MedienzuweisungTasksDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * ELN Search-API DEV
 *
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.1.3
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * MedienzuweisungTasksDto Class Doc Comment
 *
 * @category Class
 * @description DTO for Media Assignment Task.
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class MedienzuweisungTasksDto implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'MedienzuweisungTasksDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'int',
        'dealer_id' => 'int',
        'dealer_id_files' => 'int',
        'market' => 'string',
        'copy_all' => 'bool',
        'priority' => 'int',
        'caller' => 'string',
        'task_name' => 'string',
        'reserved' => 'bool',
        'reserved_by' => 'int',
        'process_id' => 'int',
        'skipped' => 'bool',
        'success' => 'bool',
        'report' => 'string',
        'date_created' => '\DateTime',
        'date_started' => '\DateTime',
        'date_terminated' => '\DateTime'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'id' => 'int32',
        'dealer_id' => 'int32',
        'dealer_id_files' => 'int32',
        'market' => null,
        'copy_all' => null,
        'priority' => 'int32',
        'caller' => null,
        'task_name' => null,
        'reserved' => null,
        'reserved_by' => 'int32',
        'process_id' => 'int32',
        'skipped' => null,
        'success' => null,
        'report' => null,
        'date_created' => 'date-time',
        'date_started' => 'date-time',
        'date_terminated' => 'date-time'
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'dealer_id' => 'dealerId',
        'dealer_id_files' => 'dealerIdFiles',
        'market' => 'market',
        'copy_all' => 'copyAll',
        'priority' => 'priority',
        'caller' => 'caller',
        'task_name' => 'taskName',
        'reserved' => 'reserved',
        'reserved_by' => 'reservedBy',
        'process_id' => 'processId',
        'skipped' => 'skipped',
        'success' => 'success',
        'report' => 'report',
        'date_created' => 'dateCreated',
        'date_started' => 'dateStarted',
        'date_terminated' => 'dateTerminated'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'dealer_id' => 'setDealerId',
        'dealer_id_files' => 'setDealerIdFiles',
        'market' => 'setMarket',
        'copy_all' => 'setCopyAll',
        'priority' => 'setPriority',
        'caller' => 'setCaller',
        'task_name' => 'setTaskName',
        'reserved' => 'setReserved',
        'reserved_by' => 'setReservedBy',
        'process_id' => 'setProcessId',
        'skipped' => 'setSkipped',
        'success' => 'setSuccess',
        'report' => 'setReport',
        'date_created' => 'setDateCreated',
        'date_started' => 'setDateStarted',
        'date_terminated' => 'setDateTerminated'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'dealer_id' => 'getDealerId',
        'dealer_id_files' => 'getDealerIdFiles',
        'market' => 'getMarket',
        'copy_all' => 'getCopyAll',
        'priority' => 'getPriority',
        'caller' => 'getCaller',
        'task_name' => 'getTaskName',
        'reserved' => 'getReserved',
        'reserved_by' => 'getReservedBy',
        'process_id' => 'getProcessId',
        'skipped' => 'getSkipped',
        'success' => 'getSuccess',
        'report' => 'getReport',
        'date_created' => 'getDateCreated',
        'date_started' => 'getDateStarted',
        'date_terminated' => 'getDateTerminated'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['dealer_id'] = isset($data['dealer_id']) ? $data['dealer_id'] : null;
        $this->container['dealer_id_files'] = isset($data['dealer_id_files']) ? $data['dealer_id_files'] : null;
        $this->container['market'] = isset($data['market']) ? $data['market'] : null;
        $this->container['copy_all'] = isset($data['copy_all']) ? $data['copy_all'] : null;
        $this->container['priority'] = isset($data['priority']) ? $data['priority'] : null;
        $this->container['caller'] = isset($data['caller']) ? $data['caller'] : null;
        $this->container['task_name'] = isset($data['task_name']) ? $data['task_name'] : null;
        $this->container['reserved'] = isset($data['reserved']) ? $data['reserved'] : false;
        $this->container['reserved_by'] = isset($data['reserved_by']) ? $data['reserved_by'] : null;
        $this->container['process_id'] = isset($data['process_id']) ? $data['process_id'] : null;
        $this->container['skipped'] = isset($data['skipped']) ? $data['skipped'] : false;
        $this->container['success'] = isset($data['success']) ? $data['success'] : null;
        $this->container['report'] = isset($data['report']) ? $data['report'] : null;
        $this->container['date_created'] = isset($data['date_created']) ? $data['date_created'] : null;
        $this->container['date_started'] = isset($data['date_started']) ? $data['date_started'] : null;
        $this->container['date_terminated'] = isset($data['date_terminated']) ? $data['date_terminated'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param int|null $id Unique identifier for the Medienzuweisung Task.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets dealer_id
     *
     * @return int|null
     */
    public function getDealerId()
    {
        return $this->container['dealer_id'];
    }

    /**
     * Sets dealer_id
     *
     * @param int|null $dealer_id ID of the related dealer for this task.
     *
     * @return $this
     */
    public function setDealerId($dealer_id)
    {
        $this->container['dealer_id'] = $dealer_id;

        return $this;
    }

    /**
     * Gets dealer_id_files
     *
     * @return int|null
     */
    public function getDealerIdFiles()
    {
        return $this->container['dealer_id_files'];
    }

    /**
     * Sets dealer_id_files
     *
     * @param int|null $dealer_id_files ID of the dealer files associated with this task.
     *
     * @return $this
     */
    public function setDealerIdFiles($dealer_id_files)
    {
        $this->container['dealer_id_files'] = $dealer_id_files;

        return $this;
    }

    /**
     * Gets market
     *
     * @return string|null
     */
    public function getMarket()
    {
        return $this->container['market'];
    }

    /**
     * Sets market
     *
     * @param string|null $market Market associated with this task.
     *
     * @return $this
     */
    public function setMarket($market)
    {
        $this->container['market'] = $market;

        return $this;
    }

    /**
     * Gets copy_all
     *
     * @return bool|null
     */
    public function getCopyAll()
    {
        return $this->container['copy_all'];
    }

    /**
     * Sets copy_all
     *
     * @param bool|null $copy_all Indicates whether all entries should be copied.
     *
     * @return $this
     */
    public function setCopyAll($copy_all)
    {
        $this->container['copy_all'] = $copy_all;

        return $this;
    }

    /**
     * Gets priority
     *
     * @return int|null
     */
    public function getPriority()
    {
        return $this->container['priority'];
    }

    /**
     * Sets priority
     *
     * @param int|null $priority Priority level of the task.
     *
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->container['priority'] = $priority;

        return $this;
    }

    /**
     * Gets caller
     *
     * @return string|null
     */
    public function getCaller()
    {
        return $this->container['caller'];
    }

    /**
     * Sets caller
     *
     * @param string|null $caller Origin or caller that initiated this task.
     *
     * @return $this
     */
    public function setCaller($caller)
    {
        $this->container['caller'] = $caller;

        return $this;
    }

    /**
     * Gets task_name
     *
     * @return string|null
     */
    public function getTaskName()
    {
        return $this->container['task_name'];
    }

    /**
     * Sets task_name
     *
     * @param string|null $task_name Name of the task.
     *
     * @return $this
     */
    public function setTaskName($task_name)
    {
        $this->container['task_name'] = $task_name;

        return $this;
    }

    /**
     * Gets reserved
     *
     * @return bool|null
     */
    public function getReserved()
    {
        return $this->container['reserved'];
    }

    /**
     * Sets reserved
     *
     * @param bool|null $reserved Indicates whether the task has been reserved.
     *
     * @return $this
     */
    public function setReserved($reserved)
    {
        $this->container['reserved'] = $reserved;

        return $this;
    }

    /**
     * Gets reserved_by
     *
     * @return int|null
     */
    public function getReservedBy()
    {
        return $this->container['reserved_by'];
    }

    /**
     * Sets reserved_by
     *
     * @param int|null $reserved_by ID of the user who reserved the task.
     *
     * @return $this
     */
    public function setReservedBy($reserved_by)
    {
        $this->container['reserved_by'] = $reserved_by;

        return $this;
    }

    /**
     * Gets process_id
     *
     * @return int|null
     */
    public function getProcessId()
    {
        return $this->container['process_id'];
    }

    /**
     * Sets process_id
     *
     * @param int|null $process_id ID of the process associated with the task.
     *
     * @return $this
     */
    public function setProcessId($process_id)
    {
        $this->container['process_id'] = $process_id;

        return $this;
    }

    /**
     * Gets skipped
     *
     * @return bool|null
     */
    public function getSkipped()
    {
        return $this->container['skipped'];
    }

    /**
     * Sets skipped
     *
     * @param bool|null $skipped Indicates whether the task was skipped.
     *
     * @return $this
     */
    public function setSkipped($skipped)
    {
        $this->container['skipped'] = $skipped;

        return $this;
    }

    /**
     * Gets success
     *
     * @return bool|null
     */
    public function getSuccess()
    {
        return $this->container['success'];
    }

    /**
     * Sets success
     *
     * @param bool|null $success Indicates whether the task was successfully completed.
     *
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->container['success'] = $success;

        return $this;
    }

    /**
     * Gets report
     *
     * @return string|null
     */
    public function getReport()
    {
        return $this->container['report'];
    }

    /**
     * Sets report
     *
     * @param string|null $report Report generated by the task.
     *
     * @return $this
     */
    public function setReport($report)
    {
        $this->container['report'] = $report;

        return $this;
    }

    /**
     * Gets date_created
     *
     * @return \DateTime|null
     */
    public function getDateCreated()
    {
        return $this->container['date_created'];
    }

    /**
     * Sets date_created
     *
     * @param \DateTime|null $date_created Timestamp of when the task was created.
     *
     * @return $this
     */
    public function setDateCreated($date_created)
    {
        $this->container['date_created'] = $date_created;

        return $this;
    }

    /**
     * Gets date_started
     *
     * @return \DateTime|null
     */
    public function getDateStarted()
    {
        return $this->container['date_started'];
    }

    /**
     * Sets date_started
     *
     * @param \DateTime|null $date_started Timestamp of when the task was started.
     *
     * @return $this
     */
    public function setDateStarted($date_started)
    {
        $this->container['date_started'] = $date_started;

        return $this;
    }

    /**
     * Gets date_terminated
     *
     * @return \DateTime|null
     */
    public function getDateTerminated()
    {
        return $this->container['date_terminated'];
    }

    /**
     * Sets date_terminated
     *
     * @param \DateTime|null $date_terminated Timestamp of when the task was terminated.
     *
     * @return $this
     */
    public function setDateTerminated($date_terminated)
    {
        $this->container['date_terminated'] = $date_terminated;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


