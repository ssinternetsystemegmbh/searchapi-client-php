<?php
/**
 * PwkQueueIndexingStatisticsDto
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * ELN Search-API DEV
 *
 * <b>Build Time: 2025-03-06T14:15:01.313+0100</b><br><br>Host: null<br>IP: SuSApp/127.0.1.1
 *
 * The version of the OpenAPI document: 1.1.2559
 * Contact: technik@eln.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.1.3
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * PwkQueueIndexingStatisticsDto Class Doc Comment
 *
 * @category Class
 * @description Statistics for a PwkQueue indexing operation.
 * @package  Swagger\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PwkQueueIndexingStatisticsDto implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'PwkQueueIndexingStatisticsDto';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'num_entries' => 'int',
        'to_oldest_entry_seconds' => 'int',
        'oldest_queue_indexing' => '\Swagger\Client\Model\PwkQueueIndexingDto'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPIFormats = [
        'num_entries' => 'int32',
        'to_oldest_entry_seconds' => 'int64',
        'oldest_queue_indexing' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'num_entries' => 'numEntries',
        'to_oldest_entry_seconds' => 'toOldestEntrySeconds',
        'oldest_queue_indexing' => 'oldestQueueIndexing'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'num_entries' => 'setNumEntries',
        'to_oldest_entry_seconds' => 'setToOldestEntrySeconds',
        'oldest_queue_indexing' => 'setOldestQueueIndexing'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'num_entries' => 'getNumEntries',
        'to_oldest_entry_seconds' => 'getToOldestEntrySeconds',
        'oldest_queue_indexing' => 'getOldestQueueIndexing'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['num_entries'] = isset($data['num_entries']) ? $data['num_entries'] : null;
        $this->container['to_oldest_entry_seconds'] = isset($data['to_oldest_entry_seconds']) ? $data['to_oldest_entry_seconds'] : null;
        $this->container['oldest_queue_indexing'] = isset($data['oldest_queue_indexing']) ? $data['oldest_queue_indexing'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets num_entries
     *
     * @return int|null
     */
    public function getNumEntries()
    {
        return $this->container['num_entries'];
    }

    /**
     * Sets num_entries
     *
     * @param int|null $num_entries The total number of entries in the queue.
     *
     * @return $this
     */
    public function setNumEntries($num_entries)
    {
        $this->container['num_entries'] = $num_entries;

        return $this;
    }

    /**
     * Gets to_oldest_entry_seconds
     *
     * @return int|null
     */
    public function getToOldestEntrySeconds()
    {
        return $this->container['to_oldest_entry_seconds'];
    }

    /**
     * Sets to_oldest_entry_seconds
     *
     * @param int|null $to_oldest_entry_seconds The duration in seconds to the oldest entry in the queue.
     *
     * @return $this
     */
    public function setToOldestEntrySeconds($to_oldest_entry_seconds)
    {
        $this->container['to_oldest_entry_seconds'] = $to_oldest_entry_seconds;

        return $this;
    }

    /**
     * Gets oldest_queue_indexing
     *
     * @return \Swagger\Client\Model\PwkQueueIndexingDto|null
     */
    public function getOldestQueueIndexing()
    {
        return $this->container['oldest_queue_indexing'];
    }

    /**
     * Sets oldest_queue_indexing
     *
     * @param \Swagger\Client\Model\PwkQueueIndexingDto|null $oldest_queue_indexing oldest_queue_indexing
     *
     * @return $this
     */
    public function setOldestQueueIndexing($oldest_queue_indexing)
    {
        $this->container['oldest_queue_indexing'] = $oldest_queue_indexing;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


