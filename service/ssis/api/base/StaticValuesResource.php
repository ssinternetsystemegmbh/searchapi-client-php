<?php
namespace service\ssis\api\base;


use Swagger\Client\Api\StaticValuesApi;

/**
 * The StaticValuesResource extended the Swagger-generated StaticValuesApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2023-02-13
 * 
 */
class StaticValuesResource extends StaticValuesApi {
    //put your code here
}
