<?php
namespace service\ssis\api\base;

use Swagger\Client\Api\CarParkApi;

/**
 * The DealersResource extended the Swagger-generated DealersApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-11-13
 * 
 */
class CarParkResource extends CarParkApi {
    //put your code here
}
