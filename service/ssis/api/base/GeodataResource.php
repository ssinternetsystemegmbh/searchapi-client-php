<?php
namespace service\ssis\api\base;


use Swagger\Client\Api\GeodataApi;

/**
 * The GeodataResource extended the Swagger-generated GeodataApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2023-02-13
 * 
 */
class GeodataResource extends GeodataApi {
    //put your code here
}
