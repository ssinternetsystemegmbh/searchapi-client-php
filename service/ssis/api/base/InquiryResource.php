<?php
namespace service\ssis\api\base;

use Swagger\Client\Api\InquiryApi;

/**
 * The InquiryResource extended the Swagger-generated InquiryApi class
 * This class is intended for future client features of the SSiS
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-11-13
 * 
 */
class InquiryResource extends InquiryApi {
    //put your code here
}
