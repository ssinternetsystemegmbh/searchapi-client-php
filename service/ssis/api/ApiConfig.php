<?php

namespace service\ssis\api;

/**
 * Description of ApiConfig
 * 
 * @copyright (c) S&S Internet Systeme GmbH
 * @author Martin Soisch <technik@ssis.de>
 * @since 2018-11-14
 * 
 */
class ApiConfig {

    protected $cfgApi = [
        'host' => 'https://api.ssis.de:8443/api',
        'temp_folder_path' => '/var/tmp/php/',
        'debug_file' => '/var/tmp/php_debug.log',
        'username' => '',
        'password' => '',
        'dealerid' => 0
    ];
    
    /**
     * Set the config array with values from config.php
     */
    public function __construct() {
        $configFilePath = __DIR__ . '/../../../config.php';
        
        if (file_exists($configFilePath)) {
            require $configFilePath;
            $this->cfgApi = $cfgApi; // comes from config.php
        }
    }
    
    /**
     * Set the config array with parameter value
     * 
     * @param array $aryConfig
     * @return ApiConfig
     */
    public function setConfig($aryConfig) {
        $this->cfgApi = $aryConfig;
        
        return $this;
    }
    
    /**
     * Return the configuration array
     * 
     * @return array
     */
    public function getConfig() {
        $aryConfig = $this->cfgApi;
        
        return $aryConfig;
    }
    
    /**
     * Set the host in the config array
     * 
     * @param string $host
     * @return ApiConfig
     */
    public function setHost($host) {
        $this->cfgApi['host'] = $host;
        
        return $this;
    }
    
    /**
     * Set the temp folder path in the config array
     * 
     * @param string $path
     * @return ApiConfig
     */
    public function setTempFolderPath($path) {
        $this->cfgApi['temp_folder_path'] = $path;
        
        return $this;
    }
    
    /**
     * Set the debug file path in the config array
     * 
     * @param string $file
     * @return ApiConfig
     */
    public function setDebugFile($file) {
        $this->cfgApi['debug_file'] = $file;
        
        return $this;
    }
    
    /**
     * Set the username in the config array
     * 
     * @param string $username
     * @return ApiConfig
     */
    public function setUsername($username) {
        $this->cfgApi['username'] = $username;
        
        return $this;
    }
    
    /**
     * Set the password in the config array
     * 
     * @param string $password
     * @return ApiConfig
     */
    public function setPassword($password) {
        $this->cfgApi['password'] = $password;
        
        return $this;
    }
    
    /**
     * Set the dealer id in the config array
     * 
     * @param string $dealerId
     * @return ApiConfig
     */
    public function setDealerId($dealerId) {
        $this->cfgApi['dealerid'] = $dealerId;
        
        return $this;
    }
}
