# Swagger\Client\GeodataApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getGeodataCityByZipAndCity**](GeodataApi.md#getGeodataCityByZipAndCity) | **GET** /geodata/city | Get geodata of the passed zipcode and city. (For internal issues)



## getGeodataCityByZipAndCity

> \Swagger\Client\Model\GeodataCityResponseWithMeta getGeodataCityByZipAndCity($zipcode, $city)

Get geodata of the passed zipcode and city. (For internal issues)

Get geodata of the passed zipcode and city. (For internal issues)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\GeodataApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$zipcode = 'zipcode_example'; // string | 
$city = 'city_example'; // string | 

try {
    $result = $apiInstance->getGeodataCityByZipAndCity($zipcode, $city);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GeodataApi->getGeodataCityByZipAndCity: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zipcode** | **string**|  | [optional]
 **city** | **string**|  | [optional]

### Return type

[**\Swagger\Client\Model\GeodataCityResponseWithMeta**](../Model/GeodataCityResponseWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

