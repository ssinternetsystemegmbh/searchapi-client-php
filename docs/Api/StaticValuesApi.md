# Swagger\Client\StaticValuesApi

All URIs are relative to *https://develop-api.ssis.de:8443/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMakes**](StaticValuesApi.md#getMakes) | **GET** /value/makes | Get makes.
[**getModelsByMake**](StaticValuesApi.md#getModelsByMake) | **GET** /value/models/{make} | Get models of the passed make.



## getMakes

> \Swagger\Client\Model\MakeResponseWithMeta getMakes()

Get makes.

Get a list of vehicle makes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getMakes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getMakes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\MakeResponseWithMeta**](../Model/MakeResponseWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getModelsByMake

> \Swagger\Client\Model\ModelResponseWithMeta getModelsByMake($make)

Get models of the passed make.

Get models of the passed make.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure HTTP basic authorization: basicAuth
$config = Swagger\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Swagger\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$make = 'make_example'; // string | 

try {
    $result = $apiInstance->getModelsByMake($make);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getModelsByMake: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **make** | **string**|  |

### Return type

[**\Swagger\Client\Model\ModelResponseWithMeta**](../Model/ModelResponseWithMeta.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

