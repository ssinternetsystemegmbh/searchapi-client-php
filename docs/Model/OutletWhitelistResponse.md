# # OutletWhitelistResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outlet_whitelist_dtos** | [**\Swagger\Client\Model\OutletWhitelistDto[]**](OutletWhitelistDto.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


