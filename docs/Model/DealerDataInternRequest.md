# # DealerDataInternRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tbl_haendlerdaten** | [**\Swagger\Client\Model\DealerDataInternFieldsOfRequest**](DealerDataInternFieldsOfRequest.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


