# # UserNewPasswordRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**username** | **string** | username | 
**principal** | **string** | Principal of the request (eg. SusAdmin, PWForgotSite) | 
**user_metadata** | **string** | The request user metadata incl. ip, browser etc. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


