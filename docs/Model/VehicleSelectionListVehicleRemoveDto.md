# # VehicleSelectionListVehicleRemoveDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** | The unique identifier of the dealer. | 
**list_id** | **int** | The unique identifier of the vehicle selection list. | 
**vehicles** | [**\Swagger\Client\Model\VehicleSusuuidList**](VehicleSusuuidList.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


