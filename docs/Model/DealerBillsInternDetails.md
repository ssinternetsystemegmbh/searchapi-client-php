# # DealerBillsInternDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bill_ids** | **int[]** | A list of bill_Ids | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


