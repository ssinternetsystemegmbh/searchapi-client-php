# # FuelType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**unit** | **string** |  | [optional] 
**cost** | **float** |  | [optional] 
**is_concrete** | **bool** |  | [optional] 
**concrete_type** | [**\Swagger\Client\Model\FuelType**](FuelType.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


