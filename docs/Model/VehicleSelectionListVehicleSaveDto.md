# # VehicleSelectionListVehicleSaveDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuid** | **string** | Unique identifier for the vehicle. | [optional] 
**meta_data** | **string** | Metadata containing additional information about the vehicle. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


