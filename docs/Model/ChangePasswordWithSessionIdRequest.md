# # ChangePasswordWithSessionIdRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** | session id | 
**principal** | **string** | Principal of the request (eg. SusAdmin, PWForgotSite) | 
**password** | **string** | The new password. | [optional] 
**user_metadata** | **string** | The request user metadata incl. ip, browser etc. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


