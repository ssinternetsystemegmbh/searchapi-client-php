# # DealerSettingsInternDeleteItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings_key** | **string** | A settings_key | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


