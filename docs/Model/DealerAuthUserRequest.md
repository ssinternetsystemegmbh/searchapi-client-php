# # DealerAuthUserRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Set the user ID if you want to modify, else set it to null or 0. | [optional] 
**username** | **string** | The login username of the user. | 
**password** | **string** | The login password of the user. | 
**email** | **string** | The unique email address of the user. | 
**roles** | **string[]** | The roles of the user. | 
**enabled** | **bool** | Set if the user is allowed to login. | [default to true]

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


