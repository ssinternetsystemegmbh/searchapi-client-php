# # GeodataCityResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geodata_city_dtos** | [**\Swagger\Client\Model\GeodataCityDto[]**](GeodataCityDto.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


