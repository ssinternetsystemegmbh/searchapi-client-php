# # VehicleInternResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**\Swagger\Client\Model\VehicleIntern[]**](VehicleIntern.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


