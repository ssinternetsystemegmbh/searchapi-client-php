# # PwkQueueIndexingStatisticsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_entries** | **int** | The total number of entries in the queue. | [optional] 
**to_oldest_entry_seconds** | **int** | The duration in seconds to the oldest entry in the queue. | [optional] 
**oldest_queue_indexing** | [**\Swagger\Client\Model\PwkQueueIndexingDto**](PwkQueueIndexingDto.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


