# # MakeResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**makes** | [**\Swagger\Client\Model\MakeDto[]**](MakeDto.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


