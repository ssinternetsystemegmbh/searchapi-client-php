# # VehicleSearchHitIntern

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **string** |  | [optional] 
**date_modified** | **string** |  | [optional] 
**vehicle_type** | **string** |  | [optional] 
**vehicle_owner_id** | **int** |  | [optional] 
**offer_number** | **string** |  | [optional] 
**consumer_price** | **double** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


