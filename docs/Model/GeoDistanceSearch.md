# # GeoDistanceSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geolocation** | [**\Swagger\Client\Model\Geolocation**](Geolocation.md) |  | [optional] 
**zip** | **string** |  | [optional] 
**radius** | **int** |  | [optional] 
**calc_distance** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


