# # DealerOwnFieldsInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_own_fields_intern_add_item** | [**\Swagger\Client\Model\DealerOwnFieldsInternAddItem**](DealerOwnFieldsInternAddItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


