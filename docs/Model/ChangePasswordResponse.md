# # ChangePasswordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **string** | Email Address the change password information was sent. | 
**date_done** | **string** | Timestamp of the entry. | 
**done_by** | **string** | Principal of the entry (eg. SusAdmin, PWForgotSite) | 
**message** | **string** | Further information. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


