# # TagAndNamePair

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **string** | The tag of the item. | [optional] 
**name** | **string** | The name of the item. | [optional] 
**count** | **int** | Quantity of the values of item. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


