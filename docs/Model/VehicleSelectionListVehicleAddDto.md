# # VehicleSelectionListVehicleAddDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** | Unique identifier for the dealer. | 
**list_id** | **int** | Unique identifier for the vehicle selection list. | 
**vehicles** | [**\Swagger\Client\Model\VehicleSelectionListVehicleSaveDto[]**](VehicleSelectionListVehicleSaveDto.md) | List of vehicles to be added to the vehicle selection list. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


