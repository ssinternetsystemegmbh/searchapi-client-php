# # DealerBillsInternSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bill_year** | **string** | Leave empty for all bills. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


