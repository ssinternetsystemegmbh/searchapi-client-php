# # DealerCooperationInternDeleteItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cooperation** | **string** | The cooperation. Allowed values are: &#39;cargarantie&#39;,&#39;bank11&#39;,&#39;directline&#39;,&#39;sicherbezahlen&#39;,&#39;carmando&#39;,&#39;akfbank&#39;,&#39;gwliste&#39;,&#39;veact&#39;,&#39;bvfk&#39;,&#39;repareo&#39;,&#39;mobilityhouse&#39; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


