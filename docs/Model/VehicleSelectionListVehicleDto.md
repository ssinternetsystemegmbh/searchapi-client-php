# # VehicleSelectionListVehicleDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuid** | **string** | Unique identifier for the vehicle. | [optional] 
**meta_data** | **string** | Metadata containing additional information about the vehicle. | [optional] 
**date_created** | [**\DateTime**](\DateTime.md) | Timestamp indicating when the vehicle was added to the selection list. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


