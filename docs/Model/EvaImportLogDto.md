# # EvaImportLogDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**tbt_id** | **int** |  | [optional] 
**errortype** | **int** |  | [optional] 
**errortext** | **string** |  | [optional] 
**comment** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


