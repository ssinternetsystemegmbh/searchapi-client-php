# # DealerOwnFieldsInternResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**thr_id** | **int** | Dealer Id | [optional] 
**lsn_id** | **int** | Language Id | [optional] 
**pdftext_aktiv** | **bool** | Field &lt;B&gt;pdftext_aktiv&lt;/B&gt; will be changed only to \&quot;TRUE\&quot;, if this endpoint request field is \&quot;TRUE\&quot; .&lt;BR&gt;&lt;font color&#x3D;red&gt;Otherwise {by &lt;B&gt;missing field or a NULL value&lt;/B&gt; } it will be set to \&quot;FALSE\&quot;!&lt;/font&gt; | [optional] 
**pdftext** | **string** | Field &lt;B&gt;pdftext&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt; | [optional] 
**pdftext_verbindlich** | **string** | Field &lt;B&gt;pdftextVerbindlich&lt;/B&gt; will be changed only, if this endpoint request field is given  &lt;font color&#x3D;red&gt;and the field value isn&#39;t EMPTY!&lt;/font&gt; | [optional] 
**shp_fahrzeugangebote** | **string** | Field &lt;B&gt;shpFahrzeugangebote&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; | [optional] 
**shp_mobile** | **string** | Field &lt;B&gt;shpMobile&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; | [optional] 
**shp_autoscout** | **string** | Field &lt;B&gt;shpAutoscout&lt;/B&gt; will be changed, if this endpoint request field is given.  &lt;font color&#x3D;darkgreen&gt;EMPTY value is also allowed&lt;/font&gt; | [optional] 
**suchmaske_ueber1_aktiv** | **bool** | Field &lt;B&gt;suchmaskeUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber1&lt;/B&gt; | [optional] 
**suchmaske_ueber1** | **string** | Field &lt;B&gt;suchmaskeUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**suchmaske_ueber2_aktiv** | **bool** | Field &lt;B&gt;suchmaskeUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber2&lt;/B&gt; | [optional] 
**suchmaske_ueber2** | **string** | Field &lt;B&gt;suchmaskeUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**suchmaske_ueber3_aktiv** | **bool** | Field &lt;B&gt;suchmaskeUeber3Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;suchmaskeUeber3&lt;/B&gt; | [optional] 
**suchmaske_ueber3** | **string** | Field &lt;B&gt;suchmaskeUeber3&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;suchmaskeUeber3Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**detailseite_ueber1_aktiv** | **bool** | Field &lt;B&gt;detailseiteUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber1&lt;/B&gt; | [optional] 
**detailseite_ueber1** | **string** | Field &lt;B&gt;detailseiteUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**detailseite_ueber2_aktiv** | **bool** | Field &lt;B&gt;detailseiteUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber2&lt;/B&gt; | [optional] 
**detailseite_ueber2** | **string** | Field &lt;B&gt;detailseiteUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**detailseite_ueber1_druck_aktiv** | **bool** | Field &lt;B&gt;detailseiteUeber1DruckAktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber1Druck&lt;/B&gt; | [optional] 
**detailseite_ueber1_druck** | **string** | Field &lt;B&gt;detailseiteUeber1Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber1DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**detailseite_ueber2_druck_aktiv** | **bool** | Field &lt;B&gt;detailseiteUeber2DruckAktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;detailseiteUeber2Druck&lt;/B&gt; | [optional] 
**detailseite_ueber2_druck** | **string** | Field &lt;B&gt;detailseiteUeber2Druck&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;detailseiteUeber2DruckAktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**bestellformular_ueber1_aktiv** | **bool** | Field &lt;B&gt;bestellformularUeber1Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;bestellformularUeber1&lt;/B&gt; | [optional] 
**bestellformular_ueber1** | **string** | Field &lt;B&gt;bestellformularUeber1&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber1Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 
**bestellformular_ueber2_aktiv** | **bool** | Field &lt;B&gt;bestellformularUeber2Aktiv&lt;/B&gt; will be set depends on user value of &lt;B&gt;bestellformularUeber2&lt;/B&gt; | [optional] 
**bestellformular_ueber2** | **string** | Field &lt;B&gt;bestellformularUeber2&lt;/B&gt; will be set only, if this endpoint request field is given.&lt;BR&gt;&lt;font color&#x3D;red&gt;An EMPTY field value is also allowed and this deactivated the field &lt;B&gt;bestellformularUeber2Aktiv&lt;B&gt; &lt;/font&gt;&lt;BR&gt; | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


