# # SendCarkparkEmail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sendername** | **string** | Name of the sender. | [optional] 
**senderemail** | **string** | Email of the sender. | [optional] 
**recipientname** | **string** | Name of the recipient. | [optional] 
**recipientemail** | **string** | Email of the recipient. | [optional] 
**subject** | **string** | Email subject. | [optional] 
**body** | **string** | Email body. | [optional] 
**susuuids** | **string[]** | List of vehicle susuuids from the carpark. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


