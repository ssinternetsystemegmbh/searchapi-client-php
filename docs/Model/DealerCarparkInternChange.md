# # DealerCarparkInternChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuid** | **string** | Only one vehicle susuuid is allowed. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


