# # DealerVehicleManufacturer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** | The dealer ID. | [optional] 
**manufacturers** | [**\Swagger\Client\Model\TagAndNamePair[]**](TagAndNamePair.md) | A list of vehicle manufacturers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


