# # Tblhaendlerdaten

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firma** | **string** | Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert. | [optional] 
**firmenzusatz** | **string** |  | [optional] 
**strasse** | **string** |  | [optional] 
**lhd_id** | **int** | ID des Herkunktsland | [optional] 
**plz** | **string** |  | [optional] 
**ort** | **string** |  | [optional] 
**ansprechpartner1** | **string** |  | [optional] 
**ansprechpartner2** | **string** |  | [optional] 
**telefon1** | **string** |  | [optional] 
**telefon2** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**mobil** | **string** |  | [optional] 
**email1** | **string** |  | [optional] 
**email2** | **string** |  | [optional] 
**emailtechnic** | **string** | Technik E-Mail | [optional] 
**emailbilling** | **string** | Rechnungs E-Mail | [optional] 
**homepage** | **string** |  | [optional] 
**ustid** | **string** |  | [optional] 
**hrb** | **string** |  | [optional] 
**gesellschaftsform** | **string** |  | [optional] 
**geschaeftsfuehrer** | **string** |  | [optional] 
**registergericht** | **string** |  | [optional] 
**registernummer** | **string** |  | [optional] 
**euidentnummer** | **string** |  | [optional] 
**vvregister** | **string** |  | [optional] 
**schiedsstelle** | **string** |  | [optional] 
**bank** | **string** |  | [optional] 
**iban** | **string** |  | [optional] 
**bic** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


