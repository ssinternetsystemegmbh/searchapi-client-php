# # VehicleSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hits_found** | **int** |  | [optional] 
**aggregations** | [**map[string,\Swagger\Client\Model\VehicleSearchTagObject[]]**](array.md) |  | [optional] 
**hits** | [**\Swagger\Client\Model\VehicleSearchHit[]**](VehicleSearchHit.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


