# # OutletWhitelistRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** | Email-Address | [optional] 
**dealer_id** | **int** | Dealer-ID | [optional] 
**company** | **string** | Company | [optional] 
**homepage** | **string** | Homepage | [optional] 
**active** | **bool** | active | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


