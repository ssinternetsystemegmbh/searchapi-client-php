# # MetaResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request** | [**\Swagger\Client\Model\MetaRequestResponse**](MetaRequestResponse.md) |  | [optional] 
**build** | [**\Swagger\Client\Model\MetaBuildResponse**](MetaBuildResponse.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


