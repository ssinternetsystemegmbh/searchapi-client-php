# # FreeInquiryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_data** | [**\Swagger\Client\Model\CustomerInquiryData**](CustomerInquiryData.md) |  | [optional] 
**free_inquiry_vehicle_data** | [**\Swagger\Client\Model\FreeInquiryVehicleData**](FreeInquiryVehicleData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


