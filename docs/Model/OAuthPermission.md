# # OAuthPermission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permission** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**invisible_to_client** | **bool** |  | [optional] 
**http_verbs** | **string[]** |  | [optional] 
**uris** | **string[]** |  | [optional] 
**default_permission** | **bool** |  | [optional] 
**default** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


