# # GetInquiriesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed** | **bool** |  | [optional] 
**seen** | **bool** |  | [optional] 
**sent** | **bool** |  | [optional] 
**from_date** | **string** |  | [optional] 
**to_date** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


