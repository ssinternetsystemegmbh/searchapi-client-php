# # Change

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**before** | **map[string,string]** |  | [optional] 
**after** | **map[string,string]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


