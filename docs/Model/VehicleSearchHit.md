# # VehicleSearchHit

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_id** | **string** |  | [optional] 
**date_modified** | **string** |  | [optional] 
**vehicle_type** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


