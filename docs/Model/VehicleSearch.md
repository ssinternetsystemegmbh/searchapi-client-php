# # VehicleSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_search** | [**\Swagger\Client\Model\DealerSearch**](DealerSearch.md) |  | [optional] 
**dealer_id_caller** | **int** |  | [optional] 
**model_search** | [**\Swagger\Client\Model\ModelSearch[]**](ModelSearch.md) |  | [optional] 
**outlet_search** | [**\Swagger\Client\Model\OutletSearch[]**](OutletSearch.md) |  | [optional] 
**offer_numbers** | **string[]** |  | [optional] 
**body_type_group_tags** | **string[]** |  | [optional] 
**body_type_tags** | **string[]** |  | [optional] 
**order_status** | **string[]** |  | [optional] 
**fuel_types** | **string[]** |  | [optional] 
**transmission_types** | **string[]** |  | [optional] 
**propulsion_types** | **string[]** |  | [optional] 
**gear_count** | **int[]** |  | [optional] 
**axe_count** | **int[]** |  | [optional] 
**door_count** | **int[]** |  | [optional] 
**door_count_range** | **int[]** |  | [optional] 
**seat_count** | **int[]** |  | [optional] 
**seat_count_range** | **int[]** |  | [optional] 
**exterior_colors** | **string[]** |  | [optional] 
**power_range** | **int[]** |  | [optional] 
**weight_range** | **int[]** |  | [optional] 
**price_range** | **float[]** |  | [optional] 
**consumption_range** | **float[]** | HINWEIS: dieses Suchfeld ist veraltet. | [optional] 
**emission_range** | **float[]** | Hinweis: Dieses Feld ist veraltet. Bitte nutzen Sie das neue Feld \&quot;wltpCombinedEmissionRange\&quot; | [optional] 
**mileage_range** | **int[]** |  | [optional] 
**euronorm_tag** | **string[]** |  | [optional] 
**co2_efficiency** | **string[]** | Hinweis: Dieses Feld ist veraltet. Bitte nutzen Sie das neue Feld \&quot;wltpCombinedCO2Class\&quot; | [optional] 
**wltp_consumption_range** | **float[]** | HINWEIS: dieses Suchfeld ist veraltet. | [optional] 
**wltp_emission_range** | **float[]** | HINWEIS: dieses Suchfeld ist veraltet. Benutzen Sie bitte wltpCombinedEmissionRange. | [optional] 
**wltp_combined_emission_range** | **float[]** | Filterung auf den gegebenen Bereich für \&quot;WLTP CO2 Emission kombiniert\&quot;. Genutzt wird der höchste und der niedrigste mitgelieferte Wert.  [g/km] | [optional] 
**pollution_badge_tag** | **string[]** |  | [optional] 
**equipment_flags** | [**\Swagger\Client\Model\EquipmentFlags**](EquipmentFlags.md) |  | [optional] 
**tax_reportable** | **bool** |  | [optional] 
**reimport** | **bool** |  | [optional] 
**one_day_registration** | **bool** |  | [optional] 
**no_previous_registration** | **bool** |  | [optional] 
**accidented** | **bool** |  | [optional] 
**require_images** | **bool** |  | [optional] 
**owned_vehicles_only** | **bool** |  | [optional] 
**owned_and_available_vehicles_only** | **bool** |  | [optional] 
**deliverable_immediately** | **bool** |  | [optional] 
**deliverable_on_short_notice** | **bool** |  | [optional] 
**original_manufacturer** | **string** |  | [optional] 
**model** | **string** |  | [optional] 
**original_series** | **string** |  | [optional] 
**hsn** | **int[]** |  | [optional] 
**tsn** | **string[]** |  | [optional] 
**vin** | **string[]** |  | [optional] 
**airbag_count** | **int[]** |  | [optional] 
**date_modified** | **string[]** |  | [optional] 
**upholstery_type_tag** | **string[]** |  | [optional] 
**price_dealer** | **float[]** |  | [optional] 
**price_consumer** | **float[]** |  | [optional] 
**first_registration_date** | **string[]** |  | [optional] 
**engine_power_kw_primary** | **int[]** |  | [optional] 
**vehicle_age** | **string[]** |  | [optional] 
**manufacturing_year** | **int[]** |  | [optional] 
**calculated** | **bool** |  | [optional] 
**calculable** | **bool** |  | [optional] 
**age_category** | **string[]** | Bestellstatus | [optional] 
**age_category_tag** | **string[]** | Bestellstatus TAG | [optional] 
**auctionhidden** | **bool** | Auktion versteckt | [optional] 
**nonsmoker** | **bool** | Nichtraucherfahrzeug | [optional] 
**include_wltp_obligatory** | **bool** | Rückgabe von WLTP-Pflichtigen \&quot;Verbrenner\&quot; Fahrzeugen | [optional] 
**include_wltp_obligatory_second_stage** | **bool** | Rückgabe von WLTP-Pflichtigen Fahrzeugen. (Schnittst.Vers ECM 24.0) [Hinweis:  \&quot;false\&quot;: Nutzung des ggf. gesetzten Filters \&quot;includeWltpObligatory\&quot;  |  \&quot;true\&quot;: alle WLTP-Pflichtigen Fahrzeuge inkl. Brennstoff-, Hybrid- und Elektro-Fahrzeuge.] | [optional] 
**wltp_efficiency** | **string[]** | Hinweis: Feld ist veraltet. Benutzen Sie bitte \&quot;wltpCombinedCO2Class\&quot;. [WLTP Effizienz (A, B, C, D, E, F, G)] | [optional] 
**wltp_combined_co2_class** | **string[]** | WLTP Kombinierte CO2 Klasse (A, B, C, D, E, F, G) | [optional] 
**pagination_parameters** | [**\Swagger\Client\Model\PaginationParameters**](PaginationParameters.md) |  | [optional] 
**aggregation_types** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


