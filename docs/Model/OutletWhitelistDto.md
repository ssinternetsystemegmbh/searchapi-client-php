# # OutletWhitelistDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** | Email-Address | [optional] 
**dealer_id** | **int** | Dealer-ID | [optional] 
**company** | **string** | Company | [optional] 
**homepage** | **string** | Homepage | [optional] 
**active** | **bool** | active | [optional] 
**date_modified** | [**\DateTime**](\DateTime.md) | Date of modification. | [optional] 
**date_created** | [**\DateTime**](\DateTime.md) | Date of creation. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


