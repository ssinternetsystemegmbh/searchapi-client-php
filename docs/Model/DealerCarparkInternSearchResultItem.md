# # DealerCarparkInternSearchResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuid** | **string** |  | [optional] 
**date_parked** | **string** |  | [optional] 
**offerdealer_id** | **int** |  | [optional] 
**offernumber_original** | **string** |  | [optional] 
**date_occurrence** | **string** |  | [optional] 
**date_recurrence** | **string** |  | [optional] 
**manufacturer_id** | **int** |  | [optional] 
**manufacturer** | **string** |  | [optional] 
**seriestype_id** | **int** |  | [optional] 
**seriestype** | **string** |  | [optional] 
**version_original** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


