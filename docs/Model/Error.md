# # Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | **string** |  | [optional] 
**key** | **string** |  | [optional] 
**message** | **string** |  | [optional] 
**rejected** | **bool** |  | [optional] 
**fields** | **map[string,string]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


