# # InquirySearchListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **int** | Total hits. | [optional] 
**inquiry_search_response_list** | [**\Swagger\Client\Model\InquirySearchResponse[]**](InquirySearchResponse.md) | List of inquiries. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


