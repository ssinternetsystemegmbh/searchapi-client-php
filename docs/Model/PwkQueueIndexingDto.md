# # PwkQueueIndexingDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier of the indexing entry. | [optional] 
**principal** | **string** | Principal name associated with the entry. | [optional] 
**distributor_id** | **int** | Identifier of the distributor associated with the entry. | [optional] 
**num_resellers** | **int** | Number of resellers associated with the indexing. | [optional] 
**cnt_reseller** | **int** | Count of resellers processed. | [optional] 
**num_chunks** | **int** | Number of chunks to be indexed. | [optional] 
**cnt_chunks** | **int** | Number of chunks processed. | [optional] 
**date_created** | [**\DateTime**](\DateTime.md) | Timestamp for when the entry was created. | [optional] 
**date_updated** | [**\DateTime**](\DateTime.md) | Timestamp for the last update of the entry. | [optional] 
**indexing_finished** | **bool** | Indicator whether indexing is finished. | [optional] 
**media_processing_active** | **bool** | Indicator if media processing is active. | [optional] 
**date_vehicle_media_indexing** | [**\DateTime**](\DateTime.md) | Timestamp for vehicle media indexing. | [optional] 
**mts_date_terminated** | [**\DateTime**](\DateTime.md) | Timestamp when the task was terminated by the MTS. | [optional] 
**type** | **string** | Type of indexing for the entry. | [optional] 
**status** | **string** | Current status of the indexing entry. | [optional] 
**comment** | **string** | Optional comment or additional information. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


