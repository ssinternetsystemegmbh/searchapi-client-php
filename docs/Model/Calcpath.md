# # Calcpath

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tax** | **float** |  | [optional] 
**distributor_top_offer_discount** | **float** |  | [optional] 
**distributor_top_offer_discount_factor** | **float** |  | [optional] 
**distributor_discount** | **float** |  | [optional] 
**base_price_with_tax** | **float** |  | [optional] 
**discounted_base_price** | **float** |  | [optional] 
**calculated_price_net** | **float** |  | [optional] 
**top_offer_discount** | **float** |  | [optional] 
**vehicle_is_top_offer** | **bool** |  | [optional] 
**price_group_surcharge_absolute** | **float** |  | [optional] 
**price_group_surcharge_relative** | **float** |  | [optional] 
**top_offer_reseller_discount** | **float** |  | [optional] 
**distributor_surcharge** | **float** |  | [optional] 
**final_gross_price** | **float** |  | [optional] 
**rounding** | **float** |  | [optional] 
**final_gross_price_rounded** | **float** |  | [optional] 
**final_gross_price_promotioned** | **float** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


