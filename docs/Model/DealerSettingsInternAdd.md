# # DealerSettingsInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings_key** | **string** | Only one settings entry is allowed. | [optional] 
**settings_value** | **string** | Blank settings value is also allowed. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


