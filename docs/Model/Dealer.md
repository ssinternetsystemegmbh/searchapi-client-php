# # Dealer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** |  | [optional] 
**headquarter_id** | **int** |  | [optional] 
**branch_ids** | **int[]** |  | [optional] 
**products** | [**\Swagger\Client\Model\Product[]**](Product.md) |  | [optional] 
**cooperations** | [**\Swagger\Client\Model\Cooperation[]**](Cooperation.md) |  | [optional] 
**businesscard_images** | [**\Swagger\Client\Model\BusinesscardImage[]**](BusinesscardImage.md) |  | [optional] 
**custom_content** | [**\Swagger\Client\Model\CustomContent[]**](CustomContent.md) |  | [optional] 
**company** | **string** |  | [optional] 
**company_additional** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**zip** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**country_iso** | **string** |  | [optional] 
**name** | **string[]** |  | [optional] 
**telephone** | **string[]** |  | [optional] 
**mobile** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**email** | **string[]** |  | [optional] 
**email_technics** | **string** |  | [optional] 
**email_billing** | **string** |  | [optional] 
**homepage** | **string** |  | [optional] 
**business_organisation** | **string** |  | [optional] 
**managing_director** | **string** |  | [optional] 
**eu_ident_number** | **string** |  | [optional] 
**ust_id** | **string** |  | [optional] 
**commercial_register** | **string** |  | [optional] 
**commercial_register_number** | **string** |  | [optional] 
**commercial_register_court** | **string** |  | [optional] 
**arbitration_office** | **string** |  | [optional] 
**insurance_broker_number** | **string** | The dealer&#39;s insurance broker number. | [optional] 
**geolocation** | [**\Swagger\Client\Model\Geolocation**](Geolocation.md) |  | [optional] 
**date_created** | **string** |  | [optional] 
**date_modified** | **string** |  | [optional] 
**bic** | **string** |  | [optional] 
**iban** | **string** |  | [optional] 
**bank** | **string** |  | [optional] 
**has_own_vehicles** | **bool** |  | [optional] 
**num_own_vehicles** | **int** |  | [optional] 
**vat** | **float** | The vat rate of the dealer&#39;s country. | [optional] 
**dealer_settings** | **map[string,string]** |  | [optional] 
**dealer_settings_from_default** | **string[]** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


