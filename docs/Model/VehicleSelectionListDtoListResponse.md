# # VehicleSelectionListDtoListResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicle_selection_list_dtos** | [**\Swagger\Client\Model\VehicleSelectionListDto[]**](VehicleSelectionListDto.md) | A list of VehicleSelectionListDto objects, each containing details of a specific vehicle selection list. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


