# # DealerOwnFieldsInternResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | [optional] 
**message** | **string** |  | [optional] 
**dealer_own_fields_intern_result_item** | [**\Swagger\Client\Model\DealerOwnFieldsInternResultItem**](DealerOwnFieldsInternResultItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


