# # ClientRegistration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**update_count** | **map[string,object]** |  | [optional] 
**token_endpoint_auth_method** | **string** |  | [optional] 
**client_name** | **string** |  | [optional] 
**grant_types** | **string[]** |  | [optional] 
**application_type** | **string** |  | [optional] 
**redirect_uris** | **string[]** |  | [optional] 
**client_uri** | **string** |  | [optional] 
**logo_uri** | **string** |  | [optional] 
**resource_uris** | **string[]** |  | [optional] 
**response_types** | **string[]** |  | [optional] 
**contacts** | **string[]** |  | [optional] 
**policy_uri** | **string** |  | [optional] 
**tos_uri** | **string** |  | [optional] 
**scope** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


