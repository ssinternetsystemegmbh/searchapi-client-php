# # DirectInquiryIntern

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer_data** | [**\Swagger\Client\Model\InquiryCustomerData**](InquiryCustomerData.md) |  | [optional] 
**detail_data** | [**\Swagger\Client\Model\DirectInquiryDetail[]**](DirectInquiryDetail.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


