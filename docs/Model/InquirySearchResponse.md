# # InquirySearchResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inquiry_id** | **int** | The ID of the inquiry. | [optional] 
**inquiry_type** | **string** | Inquiry type: &#39;Direkte Suchanfrage&#39;,&#39;Weiterleitung&#39;,&#39;Freie Suchanfrage&#39;,&#39;Unbekannt&#39; | [optional] 
**customer_salutation** | **string** | Salutation of the customer. | [optional] 
**customer_surname** | **string** | Lastname of the customer. | [optional] 
**customer_forename** | **string** | Firstname of the customer. | [optional] 
**full_name** | **string** | Composite name of the customer. | [optional] 
**vehicle_make** | **string** | Make of the vehicle. | [optional] 
**vehicle_model** | **string** | Model of the vehicle. | [optional] 
**vehicle_series** | **string** | Series of the vehicle. | [optional] 
**full_vehicle** | **string** | Composite of the vehicle. | [optional] 
**is_processed** | **bool** | Inquiry is processed. | [optional] 
**is_seen** | **bool** | Inquiry is seen. | [optional] 
**is_sent** | **bool** | Inquiry is sent. | [optional] 
**date_created** | **string** | Created date of the inquiry. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


