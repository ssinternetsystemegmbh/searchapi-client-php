# # DealerBillsInternSearchResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bill_id** | **int** |  | [optional] 
**amount_netto** | **double** |  | [optional] 
**amount_brutto** | **double** |  | [optional] 
**filename** | **string** |  | [optional] 
**billing_year** | **string** |  | [optional] 
**billing_month** | **string** |  | [optional] 
**is_debit** | **int** |  | [optional] 
**is_storno** | **int** |  | [optional] 
**date_sent** | **string** |  | [optional] 
**date_created** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


