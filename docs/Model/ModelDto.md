# # ModelDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **string** | The unique TAG. | [optional] 
**value** | **string** | The model name. | [optional] 
**exotic** | **bool** | Defines whether a model is exotic. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


