# # MediaFile

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filename** | **string** |  | [optional] 
**file_type** | **string** |  | [optional] 
**position** | **int** |  | [optional] 
**is_branded** | **bool** |  | [optional] 
**is_censored** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


