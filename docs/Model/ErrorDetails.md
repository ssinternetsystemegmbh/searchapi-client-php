# # ErrorDetails

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | [**\DateTime**](\DateTime.md) | The timestamp of the thrown error. | 
**message** | **string** | The error message. | 
**details** | **string** | Some error details. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


