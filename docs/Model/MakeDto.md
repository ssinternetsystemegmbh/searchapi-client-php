# # MakeDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tag** | **string** | The unique TAG used e.g. for the models endpoint. | [optional] 
**value** | **string** | The make name. | [optional] 
**exotic** | **bool** | Defines whether a make is exotic. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


