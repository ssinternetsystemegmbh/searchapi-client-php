# # UserNewPasswordResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | **string** | Email Address the reset token was sent. | 
**date_created** | **string** | Timestamp of the entry. | 
**created_by** | **string** | Principal of the entry (eg. SusAdmin, PWForgotSite) | 
**new_token** | **bool** | Further information. | [optional] 
**message** | **string** | Further information. | [optional] 
**token_lifetime** | **int** | Further information. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


