# # MedienzuweisungTasksStatisticsDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**num_entries** | **int** | The total number of media assignment tasks | [optional] 
**to_oldest_entry_seconds** | **int** | The duration in seconds to the oldest task entry | [optional] 
**oldest_medienzuweisung_task** | [**\Swagger\Client\Model\MedienzuweisungTasksDto**](MedienzuweisungTasksDto.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


