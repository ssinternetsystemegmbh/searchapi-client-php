# # InquiryCustomerData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** | The Customer Company if purchase_type &#x3D; &#39;geschaeftlich&#39; | [optional] 
**salutation** | **string** | The CustomerSalutation as String, use Herr, Frau, Divers, Firma, Familie, keine, If nothing matches, &#39;keine&#39; is assumed | [optional] [default to 'keine']
**name** | **string** | The Lastname as String limited by 50 digits [required] | 
**firstname** | **string** | The firstname as String limited by 50 digits | [optional] 
**street** | **string** | The Customer Resident Street and House number as String limited by 50 digits [required] | 
**postcode** | **string** | The Postcode as String, limited by 10 digits [required] | 
**city** | **string** | The City as String limited by 50 digits [required] | 
**country** | **string** | The Country as String, limited by 50 digits [required] | 
**telefon_daytime** | **string** | The Phone number at daytime as String, limited by 50 digits [required] | 
**telefon_evening** | **string** | The Phone number at evening as String, limited by 50 digits | [optional] 
**email** | **string** | The Customer Email Address, as String limited by 50 digits [required] | 
**fax** | **string** | The fax number as String, limited by 50 digits | [optional] 
**age** | **string** | The Customer age as String | [optional] 
**profession** | **string** | The Customer profession as String, limited by 50 digits | [optional] 
**purchase_type** | **string** | The desired purchase Type as PurchasingTyp &#39;privat, geschaeftlich&#39; [required] | [optional] 
**privacy_protection** | **bool** | The Customer accept our privacy protection rules, true or false mandatory, only agreed Request will be stored and processed. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


