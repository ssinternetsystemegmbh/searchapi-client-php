# # DealerCooperationInternAdd

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cooperation** | **string** | Only one cooperation entry is allowed. Allowed values are: &#39;cargarantie&#39;,&#39;bank11&#39;,&#39;directline&#39;,&#39;sicherbezahlen&#39;,&#39;carmando&#39;,&#39;akfbank&#39;,&#39;gwliste&#39;,&#39;veact&#39;,&#39;bvfk&#39;,&#39;repareo&#39;,&#39;mobilityhouse&#39; | [optional] 
**status** | **int** | Valid &#x60;status&#x60; values are: 0 (DEFAULT!) - kein Teiln. | 1 - Freig. | 2 - denied | 3 - Teiln./aktiv | 4 - gekünd. | 5 - GSG/eigene | 6 - GSG/eigene Reparturkostenversicherung | [optional] 
**user_id** | **string** | VARCHAR(64) | [optional] 
**email** | **string** | VARCHAR(64) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


