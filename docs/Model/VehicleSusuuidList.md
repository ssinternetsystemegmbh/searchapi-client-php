# # VehicleSusuuidList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**susuuids** | **string[]** | A list of SUSUUIDs uniquely identifying the associated vehicles. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


