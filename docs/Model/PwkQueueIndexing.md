# # PwkQueueIndexing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**principal** | **string** |  | [optional] 
**distributor_id** | **int** |  | [optional] 
**num_resellers** | **int** |  | [optional] 
**cnt_reseller** | **int** |  | [optional] 
**num_chunks** | **int** |  | [optional] 
**cnt_chunks** | **int** |  | [optional] 
**date_created** | [**\DateTime**](\DateTime.md) |  | [optional] 
**date_updated** | [**\DateTime**](\DateTime.md) |  | [optional] 
**indexing_finished** | **bool** |  | [optional] 
**media_processing_active** | **bool** |  | [optional] 
**date_vehicle_media_indexing** | [**\DateTime**](\DateTime.md) |  | [optional] 
**mts_date_terminated** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | [**\Swagger\Client\Model\PwkQueueIndexingType**](PwkQueueIndexingType.md) |  | 
**status** | [**\Swagger\Client\Model\PwkQueueIndexingStatus**](PwkQueueIndexingStatus.md) |  | 
**comment** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


