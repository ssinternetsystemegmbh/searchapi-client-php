# # DealerAuthUserSaveResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The user ID. | [optional] 
**dealer_id** | **int** | The dealer ID to which the user belongs. | [optional] 
**messages** | **string[]** | A save message. | [optional] 
**changes** | [**\Swagger\Client\Model\Change[]**](Change.md) | The following changes were made for data consistency. | [optional] 
**errors** | [**\Swagger\Client\Model\Error[]**](Error.md) | The following errors occurred during processing. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


