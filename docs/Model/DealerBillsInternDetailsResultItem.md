# # DealerBillsInternDetailsResultItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bill_id** | **int** |  | [optional] 
**filename** | **string** |  | [optional] 
**pdf_data** | **string** | The content of this string value is displayed as hexcode.  (You may need to remove the leading two chars &#39;0x&#39; for further processing) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


