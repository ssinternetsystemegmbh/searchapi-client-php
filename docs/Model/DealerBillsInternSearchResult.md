# # DealerBillsInternSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** |  | [optional] 
**bill_counter** | **int** |  | [optional] 
**items** | [**\Swagger\Client\Model\DealerBillsInternSearchResultItem[]**](DealerBillsInternSearchResultItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


