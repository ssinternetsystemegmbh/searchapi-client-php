# # DealerDataInternFieldsOfRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | **string** | Firma wird nur übernommen, wenn eine Händerneuanlage(INSERT) erfolgt. Zurzeit ist nur das UPDATE erlaubt und daher wird dieses Feld ignoriert. | [optional] 
**company_additional** | **string** |  | [optional] 
**street** | **string** |  | [optional] 
**lhd_id** | **int** | ID des Herkunftsland | [optional] 
**country_iso** | **string** | Country ISO2 origin | [optional] 
**zip** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**longitude** | **string** | laengengrad | [optional] 
**latitude** | **string** | breitengrad | [optional] 
**contact_person1** | **string** | ansprechpartner1 | [optional] 
**contact_person2** | **string** | ansprechpartner2 | [optional] 
**telefone1** | **string** |  | [optional] 
**telefone2** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**mobil** | **string** |  | [optional] 
**email1** | **string** |  | [optional] 
**email2** | **string** |  | [optional] 
**email_technic** | **string** | Technik E-Mail | [optional] 
**email_billing** | **string** | Rechnungs E-Mail | [optional] 
**homepage** | **string** |  | [optional] 
**ust_id** | **string** |  | [optional] 
**commercial_register** | **string** | HRB | [optional] 
**business_organisation** | **string** | Gesellschaftsform | [optional] 
**managing_director** | **string** | Geschaeftsfuehrer | [optional] 
**commercial_register_court** | **string** | Registergericht | [optional] 
**commercial_register_number** | **string** | Registernummer | [optional] 
**eu_ident_number** | **string** | euidentnummer | [optional] 
**insurance_broker_number** | **string** | Versicherungsvermittler Register (vvregister) | [optional] 
**arbitration_office** | **string** | schiedsstelle | [optional] 
**bank** | **string** |  | [optional] 
**iban** | **string** |  | [optional] 
**bic** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


