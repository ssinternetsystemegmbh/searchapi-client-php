# # AuthUserDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The user ID. | [optional] 
**username** | **string** | The login username of the user. | [optional] 
**email** | **string** | The unique email address of the user. | [optional] 
**dealer_id** | **int** | The dealer ID to which the user belongs. | [optional] 
**roles** | **string[]** | The roles of the user. | [optional] 
**lastaccess** | **string** | The last access timestamp of the user. | [optional] 
**created** | **string** | The dataset created timestamp of the user. | [optional] 
**modified** | **string** | The dataset modified timestamp of the user. | [optional] 
**enabled** | **bool** | The access of the user. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


