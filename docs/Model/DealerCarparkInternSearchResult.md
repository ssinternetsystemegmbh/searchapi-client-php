# # DealerCarparkInternSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** |  | [optional] 
**vehicle_carpark_counter** | **int** |  | [optional] 
**items** | [**\Swagger\Client\Model\DealerCarparkInternSearchResultItem[]**](DealerCarparkInternSearchResultItem.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


