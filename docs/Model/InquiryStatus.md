# # InquiryStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**was_seen** | **bool** | Inquiry was seen by the dealer. | [optional] 
**was_processed** | **bool** | Inquiry was processed by the dealer. | [optional] 
**was_sent** | **bool** | Inquiry was sent to the dealer (immediate or scheduled). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


