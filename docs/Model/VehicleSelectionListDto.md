# # VehicleSelectionListDto

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Unique identifier for the vehicle selection list. Null for new lists. | [optional] 
**dealer_id** | **int** | The unique identifier of the dealer associated with this selection list. | 
**name** | **string** | The name of the vehicle selection list. | 
**description** | **string** | A brief description of the vehicle selection list. | [optional] 
**search_json** | **string** | Search parameters in JSON format. | [optional] 
**vehicles** | [**\Swagger\Client\Model\VehicleSelectionListVehicleDto[]**](VehicleSelectionListVehicleDto.md) | The list of vehicles included in the selection. | [optional] 
**date_created** | [**\DateTime**](\DateTime.md) | Timestamp when the selection list was created. | [optional] 
**date_modified** | [**\DateTime**](\DateTime.md) | Timestamp when the selection list was last modified. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


