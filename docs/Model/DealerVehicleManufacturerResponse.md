# # DealerVehicleManufacturerResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_vehicle_manufacturers** | [**\Swagger\Client\Model\DealerVehicleManufacturer[]**](DealerVehicleManufacturer.md) | A list of dealers with their available vehicle manufacturers. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


